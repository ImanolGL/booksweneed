<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
// composer require rbdwllr/reallysimplejwt
use ReallySimpleJWT\Token;

Route::post('/register', function () {
    $data = request()->all();

    // aquí se guarda el usuario en la base de datos

    return response()->json("", 201);
});

Route::post('/login', function () {
    $data = request()->all();

    // en $data hay claves 'email' y 'password'
    // se comprueba que el usuario es correcto

    if (
        ($data['email'] == 'mortadelo@tia.es' && $data['password'] == 'ofelia')
        || ($data['email'] == 'filemon@tia.es' && $data['password'] == 'bacterio')
        || ($data['email'] == 'vicente@tia.es' && $data['password'] == 'sacarino')
    ) {
        $payload = [
            'email' => $data['email'],
            'role' => getRole($data['email']),
        ];
        $secret = getSecret();
        $token = Token::customPayLoad($payload, $secret);
        return response()->json([
            'accessToken' => $token,
        ], 200);
    }

    return response()->json([], 401);
});

Route::get('/only-for-super', function () {
    $token = request()->bearerToken();

    if (!$token) {
        return response()->json([], 401);
    }

    $tokenData = Token::getPayload($token, getSecret());
    // var_dump($tokenData);
    if ($tokenData['role'] !== 'superintendente') {
        return response()->json([], 401);
    }

    return response()->json(['ok' => true], 200);

});

if (!function_exists('getRole')) {
    function getRole($email)
    {
        if ($email === 'vicente@tia.es') {
            return 'superintendente';
        }
        return 'agente';
    }
}
if (!function_exists('getSecret')) {
    function getSecret()
    {
        return 'sUper!secret&key·123';
    }
}*/

//USERS
Route::get('/users', function (Request $request) {
    $results = DB::select('select * from users');
    return response()->json($results, 200);
});

Route::post('/users', function () {
    $data = request()->all();

    DB::insert(
        "insert into USERS (USER_ID, NAME, SURNAME, EMAIL, PASSWORD, ADMIN)
        values (:ID, :NAME, :SURNAME, :EMAIL, :PASSWORD, :ADMIN)",
        $data
    );
});

//BOOKS
Route::get('/books', function (Request $request) {
    $results = DB::select('select * from books');
    return response()->json($results, 200);
});
Route::post('/books', function () {
    $data = request()->all();

    DB::insert(
        "insert into BOOKS (BOOK_ID, TITTLE, AUTHOR, UNITS, DESCRIPTION, AVAILABLE, URL, IMG)
        values (:BOOK_ID, :TITTLE, :AUTHOR, :UNITS, :DESCRIPTION, :AVAILABLE, :URL, :IMG)",
        $data
    );
});