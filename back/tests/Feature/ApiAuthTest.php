<?php
namespace Tests\Feature;
use Tests\TestCase;

class ApiAuthTest extends TestCase
{
    
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testRegister()
    {
        $response = $this->post('/api/register', [
            'email' => 'amparo@aro.es',
            'password' => 'filomena'
            ]);

        $response->assertStatus(201);
    }

    public function testLogin(){
        $response = $this->post('api/login',[
            'email' => 'amparo@aro.es',
            'password' => 'filomena'
        ]);
        $response->assertStatus(200);

        $this->assertArrayHasKey('accessToken', $response);
    }
}

?>
