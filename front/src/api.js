
export default {


    async loadBooksData() {
        const response = await fetch("http://127.0.0.1:8000/api/books");
        return response;
    },


    //Load users Data

    async loadUsersData() {
        const response = await fetch("http://127.0.0.1:8000/api/users");
        return response;
    },

    //Create new USER
    async createNewUser(user) {
        const controller = new AbortController();//setup abort controller
        const signal = controller.signal;//signal to pass to fetch
        await fetch(
            `http://127.0.0.1:8000/api/users`,
            {
                method: 'POST',
                signal: signal,
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(user)
            }
        );
        controller.abort();
    },
    // Check if USER is logged
    checkLogin(email, password, usersArray) {
        let user = require('./user');
        var loginCheck;
        for (user of usersArray) {
            if (user.EMAIL == email && user.PASSWORD == password) {
                loginCheck = true;
                return loginCheck;
            } else {
                loginCheck = false;
            }
        }
        return loginCheck;
    },
    //Check if USER LOGGED is ADMIN or not
    isAdmin(email, password, usersArray) {
        let user = require('./user');
        var adminCheck;
        for (user of usersArray) {
            if (user.EMAIL == email && user.PASSWORD == password && user.ADMIN == 1) {
                adminCheck = true;
                return adminCheck;
            } else {
                adminCheck = false;
            }
        }
        return adminCheck;
    },

    //Create new BOOK
    async createNewBook(book) {
        const controller = new AbortController();//setup abort controller
        const signal = controller.signal;//signal to pass to fetch
        await fetch(
            `http://127.0.0.1:8000/api/books`,
            {
                method: 'POST',
                signal: signal,
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(book)
            }
        );
        controller.abort();
    },
}