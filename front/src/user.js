class User {
    constructor(
        user_id,
        name,
        surname,
        email,
        password,
        admin) {
        this.user_id = user_id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.admin = admin;
    }
}