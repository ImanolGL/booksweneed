import Vue from "vue";
import Router from "vue-router";
import store from "./store";

Vue.use(Router);
const router = new Router({
  routes: [
    {
      path: "/updatedlists",
      component: () => import("./components/UpdatedLists/UpdatedListsPage.vue"),
      meta: { requiresAdmin: true },

    },
    {
      path: "/updatedbooklist",
      component: () => import("./components/UpdatedLists/UpdatedBookList.vue"),
      meta: { requiresAdmin: true },

    },
    {
      path: "/admin",
      component: () => import("./components/Admin/AdminPage.vue"),
      meta: { requiresAdmin: true },
    },
    {
      path: "/",
      component: function (resolve) {
        require(["@/components/Home/HomePage.vue"], resolve);
      },
    },
    {
      path: "/login",
      name: "login",
      component: function (resolve) {
        require(["@/components/Login/LoginPage.vue"], resolve);
      },
    },
    {
      path: "/principal",
      component: () => import("./components/Principal/BooksPage.vue"),
      meta: { requiresAuth: true },
    },
  ],
});
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    if (store.user == null) {
      next({
        name: "login",
      });
    } else {
      next();
    }
  } else {
    next();
  }
  if (to.meta.requiresAdmin) {
    if (store.admin == null) {
      next({
        name: "login",
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
