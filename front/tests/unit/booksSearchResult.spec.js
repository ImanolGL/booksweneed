import { shallowMount } from '@vue/test-utils'
import BooksSearchResult from '@/components/Principal/BooksSearchResult.vue'


test.skip('Crear componente BooksItem.vue', () => {
    const wrapper = shallowMount(BooksSearchResult, {
        propsData: {
            msg: 'Resultados de la búsqueda',
            showSearchResults: {
                proptype: null,
                required: true,
            },
            searchBooks: {
                proptype: null,
            },
            allBooks: {
                type: null
            }

        },
    })
    expect(wrapper.text()).toContain('Resultados de la búsqueda')
})