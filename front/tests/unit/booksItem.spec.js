import { shallowMount } from '@vue/test-utils'
import BooksItem from '@/components/Principal/BooksItem.vue'


test('Crear componente BooksItem.vue', () => {
    const wrapper = shallowMount(BooksItem, {
        propsData: {
            msg: 'Sobre el libro...',
            simpleBook: {
                type: null,
                required: null
            }
        },
    })
    expect(wrapper.text()).toContain('Sobre el libro...')
})

test('Crear un Computed para refactorizar', async () => {
    const wrapper = shallowMount(BooksItem, {
        propsData: {
            msg: 'Se cargan los libros',
            simpleBook: {
                TITTLE: 'The Clean Code',
                AUTHOR: 'Robert Martin'
            }
        },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.fullBook).toContain('The Clean Code - Robert Martin')
})



