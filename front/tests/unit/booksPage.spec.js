import { shallowMount } from '@vue/test-utils'
import BooksPage from '@/components/Principal/BooksPage.vue'


test('Crear componente BooksPage.vue', () => {
    const wrapper = shallowMount(BooksPage, {
        propsData: {
            msg: 'BooksWeNeed',
            showSearchResults: {
                proptype: null,
                required: true,
                default: false,
            },
            searchBooks: {
                proptype: null,
                default: '',
            },
            booksData: {
                ISBN: 1,
                Titulo: "Heroes",
                Autor: "M. Night Shyamalan",
                clasificacion: "Drama"
            },
        },
    })

    expect(wrapper.text()).toContain('BooksWeNeed')
})