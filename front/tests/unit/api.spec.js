
import api from '@/api'

import { finishAllAsyncTasks } from './helpers'

test('login ok', async () => {
    api._fetch = jest.fn().mockResolvedValue({
        status: 200,
        data: { accessToken: '123123123' }
    })
    expect(api.accessToken).toBe('')

    const result = await api.login('email@test.com', 'passwd')

    await finishAllAsyncTasks()

    expect(result).toEqual({ ok: true, data: { accessToken: '123123123' } })
    expect(api.accessToken).toBe('123123123')
})

test('login error', async () => {
    api._fetch = jest.fn().mockResolvedValue({
        status: 401,
    })
    api.accessToken = 'zdlkrg'


    const result = await api.login('email@test.com', 'passwd')

    await finishAllAsyncTasks()

    expect(result).toEqual({
        ok: false,
        error: 'Error de autenticacion',
    })
    expect(api.accessToken).toBe('')
})

test("profile ok", async () => {

    api._fetch = jest.fn().mockReturnValue({
        status: 200,
        data: { name: 'xxx' },
    })

    const result = await api.getProfile()

    expect(result).toEqual({
        ok: true,
        data: { name: 'xxx' }
    })

});

test("getProfile error", async () => {

    api._fetch = jest.fn().mockReturnValue({
        status: 401,
    })

    const result = await api.getProfile()

    expect(result).toEqual({
        ok: false,
    })

});
