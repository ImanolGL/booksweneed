import { shallowMount } from '@vue/test-utils'
import BooksList from '@/components/Principal/BooksList.vue'

test.skip('Crear componente BooksList.vue', () => {
    const wrapper = shallowMount(BooksList, {
        propsData: {
            msg: 'Nuestras sugerencias del mes',
            showSearchResults: {
                proptype: null,
                required: true,
            },
            booksData: {
                proptype: null,
                required: true,
            },

        },
    })
    expect(wrapper.text()).toContain('Nuestras sugerencias del mes')
})

