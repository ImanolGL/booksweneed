import { shallowMount } from '@vue/test-utils'
import LoginPage from '@/components/Login/LoginPage.vue'

import api from "@/api.js";
import { finishAllAsyncTasks, mockRouter } from './helpers'

function mountPage() {
    return shallowMount(LoginPage, {
        mocks: {
            $router: mockRouter
        }
    })
}
test('call api login ', async () => {
    api.login = jest.fn().mockResolvedValue({
        ok: true
    })

    const wrapper = mountPage()

    wrapper.vm.email = 'email@example.com'
    wrapper.vm.password = 'password'

    wrapper.find('button').trigger('click')
    await finishAllAsyncTasks()

    expect(api.login).toHaveBeenCalledWith('email@example.com', 'password')
    expect(wrapper.vm.$router.path).toBe('/principal')


})

test('show error if user is not registered ', async () => {
    api.login = jest.fn().mockResolvedValue({
        ok: false,
        error: 'Authentication error'
    })

    const wrapper = mountPage()

    wrapper.vm.email = 'email@example.com'
    wrapper.vm.password = 'password'

    wrapper.find('button').trigger('click')
    await finishAllAsyncTasks()


    expect(api.login).toHaveBeenCalledWith('email@example.com', 'password')
    expect(wrapper.vm.error).toBe('Authentication error')
    expect(wrapper.find('.error').text()).toBe('Authentication error')
    expect(wrapper.vm.email).toBe('')
    expect(wrapper.vm.password).toBe('')

})
