import { shallowMount } from "@vue/test-utils";
import MyProfilePage from "@/components/MyProfile/MyProfilePage.vue";

import api from '@/api.js'

import { finishAllAsyncTasks, mockRouter } from './helpers'

function mountPage() {
    return shallowMount(MyProfilePage, {
        mocks: {
            $router: mockRouter
        }
    })
}

test('my-profile receives data', async () => {
    api.getProfile = jest.fn().mockResolvedValue({
        ok: true,
        data: 'mis datos'
    })

    const wrapper = mountPage()

    await finishAllAsyncTasks()

    expect(api.getProfile).toHaveBeenCalledWith()
    expect(wrapper.find('#message').text()).toBe(
        'mis datos'
    )

})

test('my-profile receives error', async () => {
    api.getProfile = jest.fn().mockResolvedValue({
        ok: false,
    })

    const wrapper = mountPage()

    await finishAllAsyncTasks()

    expect(api.getProfile).toHaveBeenCalledWith()
    expect(wrapper.vm.$router.path).toBe('/login')

})