export const finishAllAsyncTasks = () => new Promise(setImmediate);
export const mockRouter = {
    path: '',
    push(path) {
        this.path = path
    }
}