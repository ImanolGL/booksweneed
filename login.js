const fetch = require('node-fetch')
const jwt = require('jsonwebtoken')

const root = 'http://localhost:3000'

const Imanol = {
    email: 'imanol@nol.es',
    password: 'pitxin'
}
const Imanol2 = {
    email: 'imanol2@nol.es',
    password: 'pitxin2'
}

async function register(user) {
    const response = await fetch(`${root}/register`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    })
    console.log(response)
    const data = await response.json()
    return data
}

async function login(email, password) {
    const response = await fetch(`${root}/login`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    })
    console.log(response)
    const data = await response.json()
    return data
}

async function db() {
    const response = await fetch(`${root}/db`)
    const data = await response.json()
    return data
}

async function main() {
    // const registrationInfo = await register(Imanol2)
    // console.log(registrationInfo)
    //console.log(await db())
    const token = (await login('imanol2@nol.es', 'pitxin2')).accessToken
    console.log(token)
}

main()
